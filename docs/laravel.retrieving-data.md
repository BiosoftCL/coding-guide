---
id: laravel.retrieving-data
title: Recuperando datos 
sidebar_label: Especificar relaciones y columnas
---

## Especificar relaciones y columnas al consultar datos

Una vez creados uno o más modelos con sus respectivas tablas de base de datos asociadas, es posible recuperar datos desde su base de datos, permitiendo la especificación de columnas y las relaciones entre tablas, reduciendo el tamaño de la consulta a realizar.

Al realizar una consulta, es necesario especificar únicamente cada una de las columnas con las cuales vamos a trabajar. Por ejemplo, si consultamos un registro individual perteneciente a un "Producto".

Mal:

```php
$product = Product::where('id', $id)->first();
```

Bien:

```php
$product = Product::where('id', $id)->first(['id', 'created_at', 'name', 'details', 'price']);
```

De la misma forma, debemos especificar las relaciones con las cuales vamos a trabajar, para esto utilizamos el método `with` que provee [Eloquent ORM](https://laravel.com/docs/4.2/eloquent#eager-loading). Por ejemplo, si queremos obtener un producto junto a sus comentarios y sus descuentos asociados. 

Mal:

```php
$product = Product::where('id', $id)->first();
$comments = $product->comments;
$discounts = $product->discounts;
```

Bien:

```php
$product = Product::where('id', $id)->with(['comments', 'discounts'])->first(['id', 'created_at' 'name', 'details', 'price']);
$comments = $product->comments;
$discounts = $product->discounts;
```