---
id: laravel.validation
title: Validaciones
sidebar_label: Validaciones
---

## Validación

Mueve validaciones desde los controladores a clases *Request*:

Mal:

```php
public function store(Request $request)
{
    $request->validate([
        'title' => 'required|unique:posts|max:255',
        'body' => 'required',
        'publish_at' => 'nullable|date',
    ]);

    ....
}
```

Bien:

```php
// app/Http/Controllers/FooController.php
public function store(FooRequest $request)
{    
    ....
}

// app/Http/Request/FooRequest.php
class FooRequest extends Request
{
    public function rules()
    {
        return [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
            'publish_at' => 'nullable|date',
        ];
    }
}
```

Validar un request desde el controlador es aceptable siempre y cuando sea una validación muy muy simple y dificilmente será modificada, para esto se debe importar el uso del trait `ValidatesRequests`

```php
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends BaseController
{
    use ValidatesRequests;
}
```

### Reglas disponibles

Para ver el listado de reglas disponibles ver el siguiente enlace [Reglas de validación disponibles](https://laravel.com/docs/master/validation#available-validation-rules)