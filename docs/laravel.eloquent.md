---
id: laravel.eloquent
title: Eloquent en vez de Query Builder
sidebar_label: Eloquent en vez de Query Builder
---

## Eloquent sobre Query Builder o string SQL

Prefiere usar Eloquent en vez de Query Builder o consultas SQL y collecciones en vez de arreglos

Eloquent permite crear consultas legibles y facilmente mantenibles. Además, Eloquent tiene excelentes herramientas integradas como eliminaciones de registros (soft deletes), eventos, consultas locales (scopes), etc.

Mal:

```sql
SELECT *
FROM `articles`
WHERE EXISTS (SELECT *
              FROM `users`
              WHERE `articles`.`user_id` = `users`.`id`
              AND EXISTS (SELECT *
                          FROM `profiles`
                          WHERE `profiles`.`user_id` = `users`.`id`)
              AND `users`.`deleted_at` IS NULL)
AND `verified` = '1'
AND `active` = '1'
ORDER BY `created_at` DESC
```

Bien:

```php
Article::has('user.profile')->verified()->latest()->get();
```
