---
id: laravel.dry
title: Don't repeat yourself (DRY)
sidebar_label: Don't repeat yourself (DRY)
---

## Don't repeat yourself (DRY)

Reutiliza el código cuando puedas. El principio de responsabilidad única te está ayudando a evitar la duplicación de código. Además, reutiliza las plantillas Blade, usa Eloquent scopes para consultas, etc.

Mal:

```php
public function getActive()
{
    return $this->where('verified', 1)->whereNotNull('deleted_at')->get();
}

public function getArticles()
{
    return $this->whereHas('user', function ($q) {
            $q->where('verified', 1)->whereNotNull('deleted_at');
        })->get();
}
```

Good:

```php
public function scopeActive($q)
{
    return $q->where('verified', 1)->whereNotNull('deleted_at');
}

public function getActive()
{
    return $this->active()->get();
}

public function getArticles()
{
    return $this->whereHas('user', function ($q) {
            $q->active();
        })->get();
}
```