---
id: laravel.service-class
title: Lógica de negocios en clases de servicios
sidebar_label: Lógica de negocios en clases de servicios
---

## Lógica de negocios en clases de servicios

Un controlador debe tener una sola responsabilidad, mueve la lógica de negocio de los controladores a clases de servicio.

Mal:

```php
public function store(Request $request)
{
    if ($request->hasFile('image')) {
        $request->file('image')->move(public_path('images') . 'temp');
    }

    ....
}
```

Bien:

```php
public function store(Request $request)
{
    $this->articleService->handleUploadedImage($request->file('image'));

    ....
}

class ArticleService
{
    public function handleUploadedImage($image)
    {
        if (!is_null($image)) {
            $image->move(public_path('images') . 'temp');
        }
    }
}
```