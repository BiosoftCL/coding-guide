---
id: laravel.fat-models-skinny-controllers
title: Modelos gordos, controladores delgados
sidebar_label: Modelos gordos, controladores delgados
---

## Modelos gordos, controladores delgados

Pon la lógica relacionada con la BD en modelos *Eloquent* o en clases *Repository* si usas *Query Builder* o consultas SQL crudas.

Mal:

```php
class ClientController extends Controller {
    public function index()
    {
        $clients = Client::verified()
            ->with(['orders' => function ($q) {
                $q->where('created_at', '>', Carbon::today()->subWeek());
            }])
            ->get();

        return view('index', ['clients' => $clients]);
    }
}
```

Bien:

```php
class ClientController extends Controller {
    public function index()
    {
        return view('index', ['clients' => $this->client->getWithNewOrders()]);
    }
}

class Client extends Model
{
    public function getWithNewOrders()
    {
        return $this->verified()
            ->with(['orders' => function ($q) {
                $q->where('created_at', '>', Carbon::today()->subWeek());
            }])
            ->get();
    }
}
```