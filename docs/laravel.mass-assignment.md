---
id: laravel.mass-assignment
title: Asignación masiva (mass assignment)
sidebar_label: Asignación masiva (mass assignment)
---

## Asignación masiva (mass assignment)

Mal:

```php
$article = new Article;
$article->title = $request->title;
$article->content = $request->content;
$article->verified = $request->verified;
// Add category to article
$article->category_id = $category->id;
$article->save();
```

Bien:

```php
// Opción 1
$category->article()->create([
    'title' => $request->title,
    'content' => $request->content,
    'verified' => $request->verified,
    'category_id' => $request->category_id,
]);

// Opción 2
$category->article()->create($request->all());
```
